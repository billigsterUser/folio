export const demos: { [key: string]: string } = {
	'33094829': 'https://alfatraining.vercel.app/',
	'39903158': 'https://www.physiotherapie-sylwia.de/',
	'42365898': 'https://ytmp3-smoky.vercel.app/',
	'29292866': 'https://virtualpro.space/',
	'38050637': 'https://agroland.pages.dev/',
}