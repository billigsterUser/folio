import 'server-only'

import { Octokit } from '@octokit/rest'
import { IGitLabProject } from '@/models/gitlab'
import { cache } from 'react'

export async function getTweetCount() {
	if (!process.env.TWITTER_BEARER_TOKEN) {
		return 0
	}

	const response = await fetch(
		'https://api.twitter.com/2/users/by/username/kadrigron?user.fields=public_metrics',
		{
			headers: {
				Authorization: `Bearer ${process.env.TWITTER_BEARER_TOKEN}`,
			},
		}
	)

	const { data } = await response.json()
	return +data.public_metrics.tweet_count
}

export const getGitlabProjects = cache(async () => {
	if (!process.env.GITLAB_API_TOKEN) {
		return null
	}

	const response = await fetch(
		`https://gitlab.com/api/v4/users/${process.env.GITLAB_USER_ID}/projects?private_token=${process.env.GITLAB_API_TOKEN}`,
	)

	if (!response.ok || response.status !== 200) { return null }

	const data = await response.json()
	return data as IGitLabProject[]
})

export const getGithubProjects = cache(async () => {
	if (!process.env.GITHUB_API_TOKEN) {
		return null
	}

	const octokit = new Octokit({ auth: process.env.GITHUB_API_TOKEN })
	const response = await octokit.rest.repos.listForUser({ username: 'KKA11010', type: 'all' })

	if (response.status !== 200) { return null }

	return response.data
})