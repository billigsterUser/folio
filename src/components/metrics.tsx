import { getGithubProjects, getGitlabProjects, getTweetCount } from '@/lib/metrics'
import { GitHubIcon, GitlabIcon, TwitterIcon } from './icons'

export const revalidate = 60 * 60 * 24

export default async function Metrics() {
	let tweetCount, gitlabProjects, githubProjects
	try {
		[tweetCount, gitlabProjects, githubProjects] = await Promise.all([
			getTweetCount(),
			getGitlabProjects(),
			getGithubProjects()
		])
	} catch (error) {
		console.error(error)
	}
	return (
		<div className='dataWrap'>
			<span className='flex mb5'>
				<TwitterIcon />
				{tweetCount ? tweetCount.toLocaleString() : 0} tweets all time
			</span>
			<span className='flex mb5'>
				<GitlabIcon />
				{gitlabProjects ? gitlabProjects.length : 0} repos on gitlab
			</span>
			<span className='flex mb5'>
				<GitHubIcon />
				{githubProjects ? githubProjects.length : 0} repos on github
			</span>
		</div>
	)
}