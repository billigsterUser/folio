import me from '../../public/imgs/avatar.jpg'
import ogImage from '../../public/imgs/ogImage.png'
import ogImage1 from '../../public/imgs/ogImage1.png'

export const name = 'Agron Kadriaj'
export const description = 'My portfolio built with Next.js, PicoCSS and Vercel.'
export const avatar = me
export const ogImgUrl = ogImage.src
export const ogImgUrl1 = ogImage1.src

export const About = () => (
	<p>
		Hey, I{'\''}m Agron. I{'\''}m learning as much as possible about <b>web development</b>.
		I prefer <b>data visualization</b> but I am also interested in what happens behind the scenes in the backend.
	</p>
)

export const Bio = () => (
	<p>
		I am enthusiastic about open source development and helping other creating modern user interfaces.
		I wrote my first code in 2017. From static websites to interactive games, I have gained a lot of experience
		along the way. I work with Typescript and my favorite frameworks are React & Next.js.
	</p>
)

export const Toolkit = () => (
	<div>
		<p>
			My toolkit also consists of the following technologies:
		</p>
		<div className='grid'>
			<div>
				<ul>
					<li>HTML</li>
					<li>CSS</li>
					<li>PHP</li>
					<li>NodeJS</li>
					<li>SQL</li>
					<li>NoSQL (MongoDB, Redis)</li>
					<li>WebAssembly</li>
				</ul>
			</div>
			<div>
				<ul>
					<li>Material UI</li>
					<li>Bootstrap</li>
					<li>PicoCSS</li>
					<li>VueJS</li>
					<li>JQuery</li>
				</ul>
			</div>
		</div>
	</div>
)