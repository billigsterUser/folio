'use client'

import Image from 'next/image'
import Link from 'next/link'
import { usePathname } from 'next/navigation'
import logo from '../../public/imgs/logo.png'

export function DeskNav() {
	return (
		<aside className='deskNav'>
			<nav>
				<NavLogo />
				<NavLinks />
			</nav>
		</aside>
	)
}

export function MobileNav() {
	return (
		<>
			<nav className='mobileNav'>
				<NavLogo />
			</nav>
			<section>
				<nav className='mobileNav'>
					<NavLinks />
				</nav>
			</section>
		</>
	)
}

function NavLogo() {
	return (
		<ul>
			<li>
				<Link href='/'>
					<Image
						src={logo}
						alt='logo'
						className='logo'
						width={50}
						height={50}
						decoding='async'
						priority
					/>
				</Link>
			</li>
		</ul>
	)
}

function NavLinks() {
	const pathname = usePathname()
	return (
		<ul>
			<li className='mobileLink'>
				<Link className={pathname === '/' ? '' : 'secondary'} href='/'>
					Home
				</Link>
			</li>
			<li className='mobileLink'>
				<Link className={pathname === '/about' ? '' : 'secondary'} href='/about'>
					About
				</Link>
			</li>
			<li className='mobileLink'>
				<Link className={pathname === '/projects' ? '' : 'secondary'} href='/projects'>
					Projects
				</Link>
			</li>
		</ul>
	)
}