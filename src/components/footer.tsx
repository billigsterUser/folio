import { ArrowIcon } from './icons'

export function DeskFooter() {
	return (
		<nav>
			<ul>
				<li>
					<a
						href='https://twitter.com/kadrigron'
						target='_blank'
						rel="noreferrer"
						className='secondary'
					>
						<ArrowIcon />
						Follow me on twitter
					</a>
				</li>
				<li>
					<a
						href='https://gitlab.com/FirstTerraner'
						target='_blank'
						rel="noreferrer"
						className='secondary'
					>
						<ArrowIcon />
						GitLab
					</a>
				</li>
				<li>
					<a
						href='https://github.com/KKA11010'
						target='_blank'
						rel="noreferrer"
						className='secondary'
					>
						<ArrowIcon />
						Github
					</a>
				</li>
			</ul>
		</nav>
	)
}

export function MobileFooter() {
	return (
		<aside>
			<DeskFooter />
		</aside>
	)
}