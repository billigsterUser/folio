export default function Hamburger({ isOpen }: { isOpen: boolean }) {
	return (
		<>
			<span className={`hambar toggleBar ${isOpen && 'toggleOpen1'}`}></span>
			<span className={`hambar toggleBar ${isOpen && 'toggleOpen2'}`}></span>
			<span className={`hambar toggleBar ${isOpen && 'toggleOpen3'}`}></span>
		</>
	)
}
