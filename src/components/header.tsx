import { IHeadProps } from '@/models/components'

export default function Header({ title, description, ogImgUrl, ogImgUrl1 }: IHeadProps) {
	return (
		<>
			<title>{title}</title>
			<meta content="width=device-width, initial-scale=1" name="viewport" />
			<meta name="description" content={description} />
			<link rel="icon" href="/imgs/logo.png" />
			{/* <link rel="canonical" href="" /> */}
			<meta property="og:title" content={title} />
			<meta property="og:description" content={description} />
			<meta property="og:image" content={ogImgUrl} />
			<meta property="og:image:url" content={ogImgUrl} />
			<meta property="og:image:width" content="648" />
			<meta property="og:image:height" content="367" />
			<meta property="og:image" content={ogImgUrl1} />
			<meta property="og:image:width" content="400" />
			<meta property="og:image:height" content="400" />
			<meta name="twitter:card" content="summary_large_image" />
			<meta name="twitter:title" content={title} />
			<meta name="twitter:description" content={description} />
			<meta name="twitter:image" content={ogImgUrl} />
		</>
	)
}