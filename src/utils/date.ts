export function formatDate(locale: string, date: Date, options?: Intl.DateTimeFormatOptions) {
	const dateFormatter = Intl.DateTimeFormat(locale, { ...options })
	return dateFormatter.format(date)
}
