import { About, avatar, Bio, Toolkit } from '@/components/info'
import Metrics from '@/components/metrics'
import Image from 'next/image'

export default async function Home() {
	return (
		<>
			<h1>Agron Kadriaj</h1>
			<About />
			<div className='infoWrap'>
				<Image
					src={avatar}
					alt="Agron Kadriaj"
					className='avatar'
					width={100}
					height={100}
					decoding='async'
					priority
				/>
				{await Metrics()}
			</div>
			<Bio />
			<Toolkit />
		</>
	)
}
