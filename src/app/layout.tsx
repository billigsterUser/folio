import '@picocss/pico/css/pico.min.css'
import '../styles/globals.css'
import AnalyticsWrapper from '@/components/analytics'
import { DeskFooter, MobileFooter } from '@/components/footer'
import { DeskNav, MobileNav } from '@/components/nav'

export default function RootLayout({
	children,
}: {
	children: React.ReactNode
}) {
	return (
		<html lang="en">
			{/*
				<head /> will contain the components returned by the nearest parent
				head.tsx. Find out more at https://beta.nextjs.org/docs/api-reference/file-conventions/head
			*/}
			<head />
			<body>
				<main className="container">
					<MobileNav />
					<div className='mainContentWrap'>
						<DeskNav />
						<div className='content-layout'>
							{children}
							<section className='mobileFooter'>
								<MobileFooter />
							</section>
							<section className='deskFooter'>
								<DeskFooter />
							</section>
						</div>
					</div>
				</main>
				<AnalyticsWrapper />
			</body>
		</html>
	)
}
