export default function AboutPage() {
	return (
		<>
			<h1>About Me</h1>
			<p>
				My name is Agron. I was born and raised in Brussels, Belgium.
				<br /><br />
				Currently, I live in Germany. I speak 4 languages – German, french, english and albanian.
				Since 2017, I focus on <b>learning</b> programming languages and <b>building</b> modern apps.
				I like to reverse engineer popular platforms.
			</p>
			<br />
			<hr />
			<br />
			<p>
				I{'\''}m passionate about many creative pursuits, including drawings, music, videography,
				building FPV racing drones, and of course, coding. I spend the most of my time developing websites and
				reading documentations about the newest technologies around software.
			</p>
			<p>
				I <b>love</b> the fact that anyone can build anything without asking for permission. I find the open source
				community very interesting. It is diverse and highly motivated. Whether it{'\''}s development or advocacy,
				open source software and other collaborative projects benefit through, and because of, <b>the community</b>.
			</p>
			<blockquote>
				{'"'}It{'\''}s not the strongest species that survives, nor the most intelligent, it{'\''}s the one most adaptable to change.{'"'}
				<footer>
					<cite>- Charles Darwin</cite>
				</footer>
			</blockquote>
			<p>
				When I{'\''}m not in the creative process, I enjoy listening to audio books and podcasts.
				To switch off, I find video games very effective.
			</p>
		</>
	)
}