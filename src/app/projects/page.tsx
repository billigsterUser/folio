import { ForkIcon, LinkIcon, PullIcon, RepoIcon, StarIcon } from '@/components/icons'
import { demos } from '@/lib/demos'
import { getGitlabProjects } from '@/lib/metrics'
import { formatDate } from '@/utils/date'
import Link from 'next/link'

export default async function AboutPage() {

	const gitlabProjects = await getGitlabProjects()

	return (
		<>
			<hgroup>
				<h1>GitLab projects</h1>
				<h2>Currently, I have <b>{gitlabProjects?.length}</b> projects on GitLab.</h2>
			</hgroup>
			{gitlabProjects && gitlabProjects
				.sort((a, b) => {
					if (new Date(a.last_activity_at) > new Date(b.last_activity_at)) {
						return -1
					}
					return 1
				})
				.map(project => (
					<div key={project.id}>
						<hgroup>
							<h2>{project.name}</h2>
							<h3>{`${project.archived ? '- Not maintained - ' : ''}${project.description}`}</h3>
						</hgroup>
						{project.visibility !== 'private' &&
							<>
								<div className='flex'>
									<StarIcon />Stars: {project.star_count}
								</div>
								<div className='flex'>
									<ForkIcon />Forks: {project.forks_count}
								</div>
							</>
						}
						<div className='flex'>
							<RepoIcon />
							<span>
								<b>Created at: </b>
								{
									formatDate(
										'en',
										new Date(project.created_at),
										{ year: 'numeric', month: 'long', day: 'numeric' }
									)
								}
							</span>
						</div>
						<div className='flex'>
							<PullIcon />
							<span>
								<b>Last updated: </b>
								{' '}
								{
									formatDate(
										'en',
										new Date(project.last_activity_at),
										{ year: 'numeric', month: 'long', day: 'numeric' }
									)
								}
							</span>

						</div>
						<div className='flex'>
							{(project.visibility !== 'private' || demos[project.id]) && <LinkIcon />}
							{project.visibility !== 'private' &&
								<span className='mr20'>
									<Link href={project.web_url} target='_blank'>Source code</Link>
								</span>
							}
							{demos[project.id] && <Link href={demos[project.id]} target='_blank'>DEMO</Link>}
						</div>
						<hr />
						<br />
					</div>
				))
			}
		</>
	)
}