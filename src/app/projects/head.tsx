import Header from '@/components/header'
import { description, name, ogImgUrl, ogImgUrl1 } from '@/components/info'

export default function Head() {
	return <Header title={`Projects | ${name}`} description={description} ogImgUrl={ogImgUrl} ogImgUrl1={ogImgUrl1} />
}